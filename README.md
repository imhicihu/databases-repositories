![stability-workinprogress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)

## Rationale

* Here, it will be found all the databases with bibliographic data created/curated/enhanced by IMHICIHU's library
* In some minor cases, a mere template for future databases with a focus in compatibility between operating systems, bibliographical formatting, language-codes and inter-exchange between open/closed software
* And lastly -courtesy by _Bitbucket_- a way to have a redundant copy of all the databases in just one place
* All of them are in `.csv UTF-8` format to minimize incompatibilities

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/databases-repositories/commits/) section for the current status

### Downloads
* Find them in the [Downloads](https://bitbucket.org/imhicihu/databases-repositories/downloads/) section
![repository.jpg](https://bitbucket.org/repo/akG5RMy/images/1522092279-repository.jpg)

### Related repositories ###

* Some repositories linked with this project:
     - [Bibliographical Searcher - Standalone app](https://bitbucket.org/imhicihu/bibliographical-searcher-stand-alone-app/)
     - [Bibliographic data on iOS devices](https://bitbucket.org/imhicihu/bibliographic-data-on-ios-devices/)
     - [IMHICIHU Digital repository](https://bitbucket.org/digital_repository/imhicihu-digital-repository/)
     - [Terrae database](https://bitbucket.org/imhicihu/terrae-database/src/master/)

### Who do I talk to? ###

* Repo owner or admin
     - Contact at `imhicihu` at `gmail` dot `com`
* Other community or team contact
     - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/databases-repositories/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/databases-repositories/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.